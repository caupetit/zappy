# README #

Zappy consists in a triptych composed by:

* A game server that creates the map
* A client (player) controlled by an AI
* A graphical client displaying the map

This project is coded in C. The graphical client use OpenGL.

### The Game ###

Matches take place in Trantor, a beautiful world full of resources, where the inhabitants, the trantorians, are fighting pacifically in order to achieve the ultimate spirit level.

These resources are food and stones, the first one keeping trantorians alive, the second ones serving their evolve.

They are divided in teams, though they can't recognize each other: he can't tell whether or not a player is in his team. They must communicate through broadcasts to gather and evolve together.

### How to use ###

Usage: ./serveur -p <port> -x <width> -y <height> -n <team> [<team>] [<team>] ... -c <nb> -t <t>

* -p port number
* -x map width
* -y map height
* -n name_team1 name_team2 ...
* -c number of client by team when the game starts (The client will automatically fork to reach the number)
* -t time divisor (the greater the divisor, the faster the game)

Usage: ./client -n <team> -p <port> [-h <hostname>]

* -n team name
* -p port
* -h host (localhost by default)

Usage: ./gfx <hostname> <port>

### Screenshots ###

![Zappy1.jpg](https://bitbucket.org/repo/GR5Ajd/images/4177331886-Zappy1.jpg)

![Zappy2.jpg](https://bitbucket.org/repo/GR5Ajd/images/1700125277-Zappy2.jpg)

![Zappy0.jpg](https://bitbucket.org/repo/GR5Ajd/images/715272071-Zappy0.jpg)